<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("shaun");

echo "Nama Hewan: " . $sheep->name . " <br>"; // "shaun"
echo "Jumlah Kaki: " . $sheep->legs . " <br>"; // 2
echo "Cold Blooded?: " . $sheep->cold_blooded . " <br><br>"; // false


$sungokong = new Ape("kera sakti");
//$sungokong->yell() // "Auooo"
echo "Nama Hewan: " . $sungokong->name . " <br>"; // "shaun"
echo "Jumlah Kaki: " . $sungokong->legs . " <br>"; // 2
echo "Cold Blooded? " . $sungokong->cold_blooded . " <br>"; // false
echo $sungokong->get_yell() . " <br><br>";

$kodok = new Frog("buduk", 4, "true");
//$kodok->jump() ; // "hop hop"
echo "Nama Hewan: " . $kodok->name . " <br>"; // "buduk"
echo "Jumlah Kaki: " . $kodok->legs . " <br>"; // 4
echo "Cold Blooded? " . $kodok->cold_blooded . " <br>"; // false
echo $kodok->get_jump() . " <br><br>";

?>